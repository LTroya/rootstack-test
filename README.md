## Rootstack Test

##### Instruction

Clone the project and cd into it.

```
git clone https://gitlab.com/LTroya/1517574ab5b311eab3de0242ac130004.git && cd 1517574ab5b311eab3de0242ac130004
```

Install dependencies:

```
npm install
```

Start the script to run the algorithm:

```
npm run start
```

Re-run the script everytime you want to test another algorithm. Sometimes the api gives 503, re-run the script if that happens.
