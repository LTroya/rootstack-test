const axios = require('axios');

exports.getPlanets = () =>  axios.get(`https://swapi.dev/api/planets/`);
