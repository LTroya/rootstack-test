const axios = require('axios');

/**
 * Get a quantity of people from the api
 *
 * @param quantity
 * @returns {Promise}
 */
exports.getPeople = (quantity = 10) =>  axios.get(`https://randomuser.me/api/?results=${quantity}&nat=us`);
