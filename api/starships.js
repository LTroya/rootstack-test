const axios = require('axios');

exports.getStarships = () =>  axios.get(`https://swapi.dev/api/starships/`);
