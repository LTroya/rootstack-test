const ALLOWED_FILMS_REGEX = /[456]/;

/**
 * Get base measurement in days for a consumables (1 year)
 *
 * @param consumables
 * @returns {number}
 */
const parseConsumable = (consumables) => {
    const measurement = /[a-zA-z]+/.exec(consumables)[0];

    switch (measurement) {
        case 'month':
        case 'months':
            return 30;
        case 'year':
        case 'years':
            return 365;
        case 'week':
        case 'weeks':
            return 7;
        default:
            return 0; // Return 0 to avoid any other consumable measurement to affect the script
    }
}

/**
 * Convert consumables to days
 *
 * @param consumables
 * @returns {number}
 */
const consumableToDays = (consumables) => {
    const number = /\d+/.exec(consumables)[0];
    const toDays = parseConsumable(consumables);

    return parseInt(number) * toDays;
};

/**
 * Check if consumable is greater than the minimum consumable
 *
 * @param minConsumables
 * @param consumables
 * @returns {boolean}
 */
const hasConsumables = (minConsumables, consumables) => {
    const currentConsumable = consumableToDays(consumables);

    return currentConsumable >= minConsumables;
};

/**
 * Check if the capacity is greater than the min capacity
 *
 * @param minCapacity
 * @param capacity
 * @returns {boolean}
 */
const hasCapacity = (minCapacity, capacity) => parseInt(capacity) >= minCapacity;

/**
 * Check if the starship belongs to any of the allowed films
 *
 * @param films
 * @returns {boolean}
 */
const isOriginal = (films) => films.some(film => ALLOWED_FILMS_REGEX.test(film));

module.exports = {
    parseConsumable,
    consumableToDays,
    hasConsumables,
    hasCapacity,
    isOriginal
}
