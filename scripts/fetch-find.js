const API = require('../api/people');
const sortByAge = (person1, person2) => person1.dob.age < person2.dob.age;

exports.fetchFind = async (age) => {
    const {data} = await API.getPeople(10);

    const person = data.results.sort(sortByAge)
        .find(person => person.dob.age > age)

    console.table([{age: person.dob.age, first: person.name.first}]);

    return person;
}

