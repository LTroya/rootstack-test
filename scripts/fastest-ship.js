const API = require('../api/starships');
const utils = require('../utils/starships')
const {hasCapacity, hasConsumables, isOriginal, consumableToDays} = utils;

exports.fastestShip = async (capacity) => {
    const {data} = await API.getStarships();

    const fattestShips = data.results
        .filter(ship => hasCapacity(capacity, ship.passengers) && hasConsumables(7, ship.consumables) && isOriginal(ship.films))
        .sort((ship1, ship2) => parseInt(ship2.max_atmosphering_speed) - parseInt(ship1.max_atmosphering_speed))
        .map(ship => ({
            name: ship.name,
            max_atmosphering_speed: ship.max_atmosphering_speed,
            passengers: ship.passengers,
            films: ship.films,
            consumables_normal: ship.consumables,
            consumables: consumableToDays(ship.consumables)
        }))

    console.table(fattestShips);

    return fattestShips;
}
