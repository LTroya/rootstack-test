const API = require('../api/planet-by-terrain');

exports.planetByTerrain = async (terrain) => {
    const {data} = await API.getPlanets();
    const planets = data.results
        .filter(planet => {
            const terrains = planet.terrain.split(', ');
            return terrains.includes(terrain.toLowerCase());
        })
        .sort((planet1, planet2) => parseInt(planet2.population) - parseInt(planet1.population))
        .map(planet => ({name: planet.name, terrain: planet.terrain, population: planet.population}))

    console.table(planets);
    return planets;
}
