const API = require('../api/people');
const sortLetters = (letters) => Object.keys(letters)
    .reduce((carry, letter) => carry.concat({letter, value: letters[letter]}), [])
    .sort((letter1, letter2) => letter2.value - letter1.value);

exports.fetchCount = async () => {
        const {data} = await API.getPeople(5);
        const letters = data.results
            .map(person => `${person.name.first} ${person.name.last}`)
            .reduce((carry, name) => {
                name.split('').forEach(letter => {
                    letter = letter.toLowerCase();
                    carry[letter] = carry[letter] || 0;
                    carry[letter]++;
                })
                return carry;
            }, {});

        console.table(sortLetters(letters)[0]);

        return sortLetters(letters)[0];
}
