const API = require('../api/people');
const sortByName = (person1, person2) => person1.name.first.localeCompare(person2.name.first);

exports.fetchOrder = async () => {
    const {data} = await API.getPeople(10);

    const names = data.results.sort(sortByName).map(person => person.name.first)
    console.table(names);
    return names;
}
