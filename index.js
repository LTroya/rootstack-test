const readline = require('readline');

const {fetchOrder} = require('./scripts/fetch-order');
const {fetchFind} = require('./scripts/fetch-find');
const {fetchCount} = require('./scripts/fetch-count');
const {fastestShip} = require('./scripts/fastest-ship');
const {planetByTerrain} = require('./scripts/planet-by-terrain');

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const lines = [
    'Which script do you want to try?',
    '1. Fetch & sort',
    '2. Fetch & find',
    '3. Fetch & count',
    '4. Fastest ship',
    '5. Planet by terrain',
    'Select your option => ',
]

rl.question(lines.join('\n'), async option => {
    switch (option) {
        case '1':
            await fetchOrder();
            rl.close();
            break;
        case '2':
            rl.question('\nEnter a age => ', async age => {
                const person = await fetchFind(age);

                if (person) {
                    console.log(`The person with a age greater than ${age} is: ${person.name.first} (${person.dob.age})`)
                } else {
                    console.log(`No person meets the criteria`)
                }

                rl.close();
            });
            break;
        case '3':
            const letter = await fetchCount();
            console.log(`The most repetitive letter is: ${letter.letter} (${letter.value})`)
            rl.close();
            break;
        case '4':
            rl.question('\nEnter a passenger capacity => ', async capacity => {
                const ships = await fastestShip(capacity);

                if (ships.length) {
                    console.log(`The ship that meets the criteria is: ${ships[0].name} (${ships[0].passengers})`)
                } else {
                    console.log(`No ship meets the criteria`)
                }

                rl.close();
            });
            break;
        case '5':
            rl.question('\nEnter a terrain type => ', async terrain => {
                const planets = await planetByTerrain(terrain);

                if (planets.length) {
                    console.log(`The planet that meets the criteria is: ${planets[0].name} (${planets[0].population})`)
                } else {
                    console.log('No planet meets the criteria')
                }

                rl.close();
            });
            break;
        default:
            rl.close();
    }
});
